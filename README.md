

#### ChromeDriver（macOS）

- 2024-09 更新
- chrome 115 以上版本需要看这个：<https://googlechromelabs.github.io/chrome-for-testing/>
  - Chrome for Testing是Chrome的一个专用版本，针对测试用例进行了优化，不会自动更新，与Chrome发布流程集成，每个Chrome版本都可用。这个版本的二进制文件尽可能接近常规的Chrome，同时不会对测试用例产生负面影响
  - 下载地址中也有对应的 chromedriver 版本也自行下载。
  - 不用下载 chrome-headless-shell 版本，因为普通的 chrome 通过代码中配置 options.addArguments("--headless"); 也可以实现

```
将 chromedriver 放置到：/usr/local/bin/
sudo mv chromedriver /usr/local/bin/

4 授权 非常重要。否则访问不了。
sudo chmod 777 /usr/local/bin/chromedriver
```

- 录制步骤可以使用：
    - [chrome 扩展：Katalon Recorder](https://chrome.google.com/webstore/detail/katalon-recorder/ljdobmomdgdljniojadhoplhkpialdid)
    - [Katalon Studio](https://www.katalon.com/)

## 服务器上部署 ChromeDriver（还没测试过）

```
步骤跟上面 macOS 一样

查看chrome版本号
google-chrome --version

将 chromedriver 放置到：/usr/local/bin/
sudo chmod 777 /usr/local/bin/chromedriver


安装一些库（如果失败了再来安装）：
yum install mesa-libOSMesa-devel gnu-free-sans-fonts wqy-zenhei-fonts -y

```

-------------------------------------------------------------------


#### geckodriver（macOS）

- java 包主页：<https://github.com/SeleniumHQ/selenium/>
- java 包仓库：<https://central.sonatype.com/artifact/org.seleniumhq.selenium/selenium-java/>
- geckodriver 下载地址：<https://github.com/mozilla/geckodriver/releases>
- 对应支持的 firefox 版本说明：<https://firefox-source-docs.mozilla.org/testing/geckodriver/Support.html>
- 它们之间的版本关系暂时理不清楚，只能通过发布的时间来推测当时的版本情况。但是又因为 firefox 是 2020 年以后才支持 apple silicon 架构的，所以只能用比较新的版本。
- 2024-09-09 经过测试：百度云 judasn2 目录下：`全部文件 > -A-分享出去的软件 > -我梳理的selenlum工具包` 中的 firefox 版本支持 apple silicon 并且可以抓取到内容

```
将 geckodriver 放置到：/usr/local/bin/
sudo mv geckodriver /usr/local/bin/

4 授权 非常重要。否则访问不了。
sudo chmod 777 /usr/local/bin/geckodriver
```

## 服务器上部署 geckodriver

```
安装软件：
sudo yum install -y firefox

查看 firefox 版本号
firefox --version

下载后，将 geckodriver 放置到：/usr/local/bin/

4 授权 非常重要。否则访问不了。
sudo chmod 777 /usr/local/bin/geckodriver
```






