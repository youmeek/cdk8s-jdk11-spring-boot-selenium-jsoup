package com.cdk8s.example.simplespringboot.properties;



import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.File;


@Setter
@Getter
@ToString
@Component
@ConfigurationProperties(prefix = "custom.properties.upload")
public class UploadParamProperties {


	// 默认值为当前用户 home 目录
	private String localUploadStoragePath = System.getProperty("user.home") + File.separator + "sculptor-boot-backend-upload-dir" + File.separator + "upload";

}
