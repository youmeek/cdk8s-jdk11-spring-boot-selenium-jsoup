package com.cdk8s.example.simplespringboot.properties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@Setter
@Getter
@ToString
@Component
@ConfigurationProperties(prefix = "sculptor.tencent.cos")
public class TencentCosParamProperties {

	private String bucketName;
	private String region;

	private String secretId;
	private String secretKey;

	private String fileSourceDomain;
	private String fileCdnDomain;


}
