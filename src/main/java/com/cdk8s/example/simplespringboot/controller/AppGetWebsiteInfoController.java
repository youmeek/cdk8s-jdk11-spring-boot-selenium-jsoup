package com.cdk8s.example.simplespringboot.controller;


import com.cdk8s.example.simplespringboot.pojo.biz.R;
import com.cdk8s.example.simplespringboot.pojo.param.AppGetWebsiteInfoRequestParam;
import com.cdk8s.example.simplespringboot.pojo.response.AppGetWebsiteInfoResponseDTO;
import com.cdk8s.example.simplespringboot.service.AppGetWebsiteInfoService;
import com.cdk8s.example.simplespringboot.utils.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Slf4j
@RestController
@RequestMapping("/multiapi")
public class AppGetWebsiteInfoController {


	@Autowired
	private AppGetWebsiteInfoService appGetWebsiteInfoService;

	// region=====================================查询业务 start=====================================

	/**
	 * 抓取链接信息
	 * {
	 *     "code": 200,
	 *     "isSuccess": true,
	 *     "msg": "操作成功",
	 *     "timestamp": 1643769693710,
	 *     "data": {
	 *         "urlTitle": "百度一下，你就知道",
	 *         "iconUrl": "https://cdn.uptmr.com/icon/2022-02/02/0205214844084202817b1cdd38bf43a0.ico"
	 *     }
	 * }
	 */
	@RequestMapping(value = "/open/getWebsiteInfo_8MmXvEBHhGxUG5rS", method = RequestMethod.POST)
	public ResponseEntity<?> getWebsiteInfo(@RequestBody AppGetWebsiteInfoRequestParam param) {
		String gotoUrl = param.getGotoUrl();
		if (StringUtil.isBlank(gotoUrl)) {
			return R.failure("参数不能为空");
		}
		AppGetWebsiteInfoResponseDTO responseDTO = appGetWebsiteInfoService.getWebsiteInfo(gotoUrl);
		if (responseDTO == null) {
			return R.failure("抓取失败");
		}
		return R.success(responseDTO);
	}

    /**
     * 抓取 apple swiftui 官网文档
     * @param param
     * @return
     */
	@RequestMapping(value = "/open/getDocumentBySeleniumInSwiftui", method = RequestMethod.POST)
	public ResponseEntity<?> getDocumentBySeleniumInSwiftui(@RequestBody AppGetWebsiteInfoRequestParam param) {
		String gotoUrl = param.getGotoUrl();
		if (StringUtil.isBlank(gotoUrl)) {
			return R.failure("参数不能为空");
		}
		Document document = appGetWebsiteInfoService.getDocumentByFirefoxSelenium(gotoUrl);
        if (document == null) {
            return R.failure("抓取失败");
        }
        Element elementById = document.getElementById("app-main");
        if (elementById == null) {
            return R.failure("无法抓取到主页面内容");
        }
        String text = elementById.text();
        // 使用正则表达式匹配所有的英文单词
        Pattern pattern = Pattern.compile("\\b[a-zA-Z]+\\b");
        Matcher matcher = pattern.matcher(text);

        // 遍历所有匹配到的英文单词
        while (matcher.find()) {
            System.out.println(matcher.group());
        }
        return R.success();
	}


	// endregion=====================================操作业务 end=====================================
	// region=====================================私有方法 start=====================================


	// endregion=====================================私有方法 end=====================================
}
