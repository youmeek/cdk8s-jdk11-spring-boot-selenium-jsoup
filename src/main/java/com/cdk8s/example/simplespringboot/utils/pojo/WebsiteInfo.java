package com.cdk8s.example.simplespringboot.utils.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Setter
@Getter
@ToString
public class WebsiteInfo {
	private String baseUrl;
	private String title;
	private String iconUrl;
	private Set<String> iconUrlSet;
	private String keywords;
	private String description;
	private Long alexaPvRank;
	private Long alexaUvRank;
}
