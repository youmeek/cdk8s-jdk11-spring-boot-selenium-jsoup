package com.cdk8s.example.simplespringboot.utils;


import java.util.HashMap;
import java.util.Map;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class SendWorkWeixinMessageUtil {

    @SneakyThrows
    public static void sendApplicationMessage(String message) {
        if (StringUtil.isBlank(message)) {
            return;
        }

        String url = "https://api.uptmr.com/upupmo-java/multiapi/open/sendWordWeixinApplicationMessage_20240906";
        Map<String, Object> bodyMap = new HashMap<>();
        bodyMap.put("message", message);
        HttpClientUtil.postJsonWithResult(url, JsonUtil.toJson(bodyMap), null);
    }

}
