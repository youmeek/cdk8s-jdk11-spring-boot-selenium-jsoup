package com.cdk8s.example.simplespringboot.utils;

import cn.hutool.http.useragent.UserAgentParser;
import com.cdk8s.example.simplespringboot.constant.GlobalConstant;


import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


public class UserAgentUtil {

	private static final String JSON_TYPE = "application/json";
	private static final String X_TYPE = "XMLHttpRequest";

	public static boolean isMobile(HttpServletRequest request) {
		return isMobile(getUserAgent(request));
	}

	public static boolean isMobile(String userAgentString) {
		return UserAgentParser.parse(userAgentString).isMobile();
	}

	public static String getUserAgent(HttpServletRequest request) {
		return request.getHeader(GlobalConstant.HTTP_HEADER_USER_AGENT);
	}

	public static String getRandomUserAgent() {
		List<String> userAgentList = new ArrayList<>();
		userAgentList.add(GlobalConstant.USER_AGENT_FIREFOX);
		userAgentList.add(GlobalConstant.USER_AGENT);
		userAgentList.add("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36 OPR/37.0.2178.32");
		userAgentList.add("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586");
		userAgentList.add("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.80 Safari/537.36 Core/1.47.277.400 QQBrowser/9.4.7658.400");
		userAgentList.add("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 UBrowser/5.6.12150.8 Safari/537.36");
		userAgentList.add("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586");
		userAgentList.add("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36 Edg/92.0.902.62");
		userAgentList.add("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36");
		int nextInt = RandomUtil.nextInt(0, userAgentList.size());
		return userAgentList.get(nextInt);
	}

	/**
	 * 判断是否是 ajax 请求类型，如果是则响应也应该是 JSON
	 * 没有指定返回类型，根据 http 请求头进行判断返回什么类型
	 * 如果是 android，ios，小程序，微信内置浏览器这类手机端也返回 json（微信内置和小程序都有：MicroMessenger 标识）
	 */
	public static boolean isJsonRequest(HttpServletRequest request) {
		// 没有指定返回类型，根据 http 请求头进行判断返回什么类型
		// 如果是 android，ios，小程序，微信内置浏览器这类手机端也返回 json（微信内置和小程序都有：MicroMessenger 标识）
		String contentTypeHeader = request.getHeader("Content-Type");
		String acceptHeader = request.getHeader("Accept");
		String xRequestedWith = request.getHeader("X-Requested-With");
		String userAgent = request.getHeader(GlobalConstant.HTTP_HEADER_USER_AGENT);
		if (StringUtil.isBlank(userAgent)) {
			// 如果没有传 userAgent 就默认是 ajax 请求
			return true;
		}

		boolean checkIsJson = (contentTypeHeader != null && StringUtil.containsIgnoreCase(contentTypeHeader, JSON_TYPE))
				|| (acceptHeader != null && StringUtil.containsIgnoreCase(acceptHeader, JSON_TYPE))
				|| StringUtil.equalsIgnoreCase(xRequestedWith, X_TYPE)
				|| StringUtil.containsIgnoreCase(userAgent, "okhttp")
				|| StringUtil.containsIgnoreCase(userAgent, "httpclient")
				|| StringUtil.containsIgnoreCase(userAgent, "miniProgram")
				|| UserAgentUtil.isMobile(userAgent)
				|| StringUtil.containsIgnoreCase(userAgent, "MicroMessenger");
		return checkIsJson;
	}

	public static String getBrowser(String userAgentString) {
		return UserAgentParser.parse(userAgentString).getBrowser().getName();
	}

	public static String getOs(String userAgentString) {
		return UserAgentParser.parse(userAgentString).getOs().getName();
	}

	public static String getPlatform(String userAgentString) {
		return UserAgentParser.parse(userAgentString).getPlatform().getName();
	}
}
