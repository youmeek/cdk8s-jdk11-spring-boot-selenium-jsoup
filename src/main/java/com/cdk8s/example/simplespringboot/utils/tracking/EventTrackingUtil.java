package com.cdk8s.example.simplespringboot.utils.tracking;

import com.cdk8s.example.simplespringboot.utils.ExceptionUtil;
import com.cdk8s.example.simplespringboot.utils.StringUtil;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;

/**
 * 关于 SiftingAppender 可以查看：https://www.jianshu.com/p/a33902d58530
 */
@Slf4j
public class EventTrackingUtil {

	private static final String MDC_KEY = "customSiftName";

	public static void log(String message) {
		log(message, "default");
	}

	public static void log(String message, String fileName) {
		if (StringUtil.isBlank(fileName)) {
			throw new RuntimeException("日志写入方式不合法，请联系管理员进行处理");
		}
		try {
			MDC.put(MDC_KEY, fileName);
			log.debug(message);
		} catch (Exception e) {
			ExceptionUtil.printStackTraceAsString(e);
		} finally {
			MDC.remove(MDC_KEY);
		}
	}

}
