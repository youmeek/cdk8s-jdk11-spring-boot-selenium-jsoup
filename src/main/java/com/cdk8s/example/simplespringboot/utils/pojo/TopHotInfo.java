package com.cdk8s.example.simplespringboot.utils.pojo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@NoArgsConstructor
@Setter
@Getter
@ToString
public class TopHotInfo {

	private String columnName;
	private String subColumnName;
	private String gotoUrl;
	private List<UrlListBean> urlList;

	@NoArgsConstructor
	@Setter
	@Getter
	@ToString
	public static class UrlListBean {
		private String href;
		private String title;
	}
}
