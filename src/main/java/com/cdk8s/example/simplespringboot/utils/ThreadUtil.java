package com.cdk8s.example.simplespringboot.utils;

import lombok.SneakyThrows;

import java.util.concurrent.TimeUnit;

/**
 * 集合工具类.
 */
public final class ThreadUtil {

	@SneakyThrows
	public static void sleepBySeconds(int seconds) {
		TimeUnit.SECONDS.sleep(seconds);
	}

	@SneakyThrows
	public static void sleepByMillis(long millis) {
		TimeUnit.MILLISECONDS.sleep(millis);
	}

	public static void getName() {
		Thread.currentThread().getName();
	}

	// =====================================私有方法 end=====================================

}



