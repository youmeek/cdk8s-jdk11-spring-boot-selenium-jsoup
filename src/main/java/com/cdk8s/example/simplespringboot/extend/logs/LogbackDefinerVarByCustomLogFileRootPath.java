package com.cdk8s.example.simplespringboot.extend.logs;

import ch.qos.logback.core.PropertyDefinerBase;
import com.cdk8s.example.simplespringboot.utils.StringUtil;
import lombok.extern.slf4j.Slf4j;

/***
 * 自定义 logback 变量值.
 */
@Slf4j
public class LogbackDefinerVarByCustomLogFileRootPath extends PropertyDefinerBase {

	@Override
	public String getPropertyValue() {
		return getUniqName();
	}

	/**
	 * 读取系统环境变量值
	 * ---- 如果本地调试，添加环境变量后需要重启 IntelliJ IDEA，不然会无法读取到。
	 * ---- 如果是 CentOS 下采用 systemd 自启动配置的话，还需要额外在 startup.sh 脚本下也增加一次环境变量
	 *
	 * vim /root/.bashrc
	 *
	 * export LOGBACK_LOG_FILE_ROOT_PATH="/opt/jar"
	 *
	 * source /root/.bashrc
	 */
	private String getUniqName() {
		String logbackLogFileRootPath = System.getenv("LOGBACK_LOG_FILE_ROOT_PATH");
		if (StringUtil.isNotBlank(logbackLogFileRootPath)) {
			return logbackLogFileRootPath;
		}
		return "./logs";
	}
}
