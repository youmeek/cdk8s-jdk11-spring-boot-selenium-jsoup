package com.cdk8s.example.simplespringboot;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@Slf4j
@SpringBootApplication
@ServletComponentScan
public class SimpleSpringBootApplication implements CommandLineRunner {

	@Value("${server.port:9091}")
	private String serverPort;

	@Value("${spring.application.name}")
	private String myvar;

	@Value("${server.servlet.context-path:/sculptor-boot-backend}")
	private String serverContextPath;

	// =================================================================================

	public static void main(String[] args) {
		SpringApplication.run(SimpleSpringBootApplication.class, args);
	}

	@Override
	public void run(String... strings) {
		log.info("=================================Application Startup Success=================================");
		log.info("myvar={}",myvar);
		log.info("index >> http://sculptor.cdk8s.com:{}{}", serverPort, serverContextPath);
		log.info("index >> http://sculptor.cdk8s.com:{}{}/multiapi/open/getDocumentBySelenium（post请求类型）", serverPort, serverContextPath);
		log.info("index >> http://sculptor.cdk8s.com:{}{}/multiapi/open/getWebsiteInfo_8MmXvEBHhGxUG5rS（post请求类型）", serverPort, serverContextPath);
	}
}
