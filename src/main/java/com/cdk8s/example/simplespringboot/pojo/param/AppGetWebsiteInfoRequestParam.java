package com.cdk8s.example.simplespringboot.pojo.param;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@NoArgsConstructor
@Setter
@Getter
@ToString(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AppGetWebsiteInfoRequestParam implements Serializable {
	private static final long serialVersionUID = -1L;


	private String gotoUrl;


}
