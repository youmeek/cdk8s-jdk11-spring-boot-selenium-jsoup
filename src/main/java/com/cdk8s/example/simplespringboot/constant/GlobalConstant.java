package com.cdk8s.example.simplespringboot.constant;


public interface GlobalConstant {

	String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36";
	String USER_AGENT_FIREFOX = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:89.0) Gecko/20100101 Firefox/89.0";
	String HTTP_HEADER_USER_AGENT = "User-Agent";

}
