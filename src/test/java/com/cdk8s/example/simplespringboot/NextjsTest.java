package com.cdk8s.example.simplespringboot;

import com.cdk8s.example.simplespringboot.utils.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import lombok.extern.slf4j.Slf4j;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@Slf4j
public class NextjsTest {

    private static final String QUERY_HTML_CLASS = "prose-vercel";

    @Test
    public void testByChrome() {
        // String readFileToString = FileUtil.readFileToString("/Users/meek/Documents/已经生成的计算机单词/IntelliJ-IDEA-已完成/documentIsNullUrlList2.json");
        // List<String> allUrlSet = JsonUtil.toList(readFileToString, new TypeReference<List<String>>() {});

        String rootParentPath = "/Users/meek/Documents/已经生成的计算机单词/nextjs-已完成";
        Set<String> allUrlSet = new HashSet<>();
        List<File> fileList = FileUtil.loopFiles(rootParentPath);
        for (File file : fileList) {
            if (!file.getName().endsWith(".csv")) {
                continue;
            }
            List<String> tempUrlList = readCsvData(file.getPath());
            allUrlSet.addAll(tempUrlList);
        }

        Set<String> documentIsNullUrlList = new HashSet<>();
        Set<String> appMainIsNullUrlList = new HashSet<>();
        Set<String> successUrlList = new HashSet<>();
        Set<String> exceptionUrlList = new HashSet<>();
        String fileSavePath = rootParentPath + "/txt";

        int flag = 0;
        int listSize = allUrlSet.size();
        for (String gotoUrl : allUrlSet) {
            flag = flag + 1;
            if (flag < 196) {
                // 基于上次继续
                continue;
            }
            log.warn("------zch------ 当前循环第 <{}>, 总数<{}>", flag, listSize);
            try {
                Document document = getDocumentByChromeSelenium(gotoUrl);
                if (document == null) {
                    documentIsNullUrlList.add(gotoUrl);
                    continue;
                }
                Elements elementsByClass = document.getElementsByClass(QUERY_HTML_CLASS);
                if (CollectionUtil.isEmpty(elementsByClass)) {
                    appMainIsNullUrlList.add(gotoUrl);
                    continue;
                }
                Element elementObject = elementsByClass.get(0);
                String text = elementObject.text();

                // 写入文件
                String audioFilePath = fileSavePath + "/" + DatetimeUtil.currentEpochMilli() + ".txt";
                FileUtil.writeStringToFile(audioFilePath, text);
                successUrlList.add(gotoUrl);

                // 睡眠2秒
                //ThreadUtil.sleepBySeconds(2);
            } catch (Exception e) {
                ExceptionUtil.printStackTraceAsString(e);
                exceptionUrlList.add(gotoUrl);
                SendWorkWeixinMessageUtil.sendApplicationMessage("jsoup 抓取失败");
            }

        }

        String documentIsNullUrlList_path = rootParentPath + "/documentIsNullUrlList.json";
        FileUtil.writeStringToFile(documentIsNullUrlList_path, JsonUtil.toJson(documentIsNullUrlList));

        String appMainIsNullUrlList_path = rootParentPath + "/appMainIsNullUrlList.json";
        FileUtil.writeStringToFile(appMainIsNullUrlList_path, JsonUtil.toJson(appMainIsNullUrlList));

        String successUrlList_path = rootParentPath + "/successUrlList.json";
        FileUtil.writeStringToFile(successUrlList_path, JsonUtil.toJson(successUrlList));

        String exceptionUrlList_path = rootParentPath + "/exceptionUrlList.json";
        FileUtil.writeStringToFile(exceptionUrlList_path, JsonUtil.toJson(exceptionUrlList));

        // 发送通知
        SendWorkWeixinMessageUtil.sendApplicationMessage("jsoup 已经抓取完成");
    }

    public List<String> readCsvData(String csvFile) {
        String line;
        String csvSplitBy = ","; // CSV 分隔符

        List<String> urlList = new ArrayList<>(); // 用于存储第一列 URL 的数组
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            // 读取表头行
            br.readLine(); // 如果不需要标题行，读取并跳过它

            while ((line = br.readLine()) != null) {
                // 使用逗号作为分隔符
                String[] columns = line.split(csvSplitBy);

                // 检查数组长度，确保第一列存在数据
                if (columns.length > 0) {
                    // 将第一列的 URL 添加到数组中
                    urlList.add(columns[0].replaceAll("\"", "")); // 去除多余的引号
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        // 输出所有的 URL
        // for (String url : urlList) {
        // System.out.println(url);
        // }
        return urlList;
    }

    public Document getDocumentByChromeSelenium(String gotoUrl) {
        System.setProperty("webdriver.chrome.bin", "/Users/meek/my-software/chrome-mac-arm64/Google Chrome for Testing.app/Contents/MacOS/Google Chrome for Testing");
        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
        WebDriver webDriver = null;
        try {
            // 设置 Chrome 选项
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--headless"); // 无浏览器模式
            options.addArguments("--disable-gpu"); // 避免一些系统问题
            options.addArguments("--no-sandbox"); // 避免一些权限问题
            options.addArguments("--disable-extensions");
            options.addArguments("user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36"); // 设置 User-Agent

            webDriver = new ChromeDriver(options);

            // 隐式等待 5 秒
            webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            webDriver.get(gotoUrl);

            // 使用 WebDriverWait 进行显式等待，确保页面中某个元素完全加载后再进行操作
            WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(5));
            WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(QUERY_HTML_CLASS)));

            // 模拟用户行为：滚动页面
            ((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0, 1000);");

            String pageHtmlSource = webDriver.getPageSource(); // 获取网页全部信息
            return Jsoup.parse(pageHtmlSource);
        } catch (Exception e) {
            log.error("------zch------chrome selenium 抓取报错 {}, <{}>", gotoUrl, e.getMessage());
        } finally {
            // 使用完毕，关闭 webDriver
            if (webDriver != null) {
                webDriver.quit();
            }
        }
        return null;
    }

}
