package com.cdk8s.example.simplespringboot;

import com.cdk8s.example.simplespringboot.utils.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import com.cdk8s.example.simplespringboot.utils.pojo.TopHotInfo;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

@Slf4j
public class AppleSwiftuiTest {

    @Test
    public void testByFirefox() {
        // 以下逻辑是针对 apple swiftui 官网文档进行调试的
        //String csvFile = "/Users/meek/Documents/已经生成的计算机单词/swfit-swfitui-已完成/swiftui.csv";
        //List<String> urlList = readCsvData(csvFile);

        String readFileToString = FileUtil.readFileToString("/Users/meek/Documents/已经生成的计算机单词/swfit-swfitui-已完成/appMainIsNullUrlList.json");
        List<String> urlList = JsonUtil.toList(readFileToString, new TypeReference<List<String>>() {});

        Set<String> documentIsNullUrlList = new HashSet<>();
        Set<String> appMainIsNullUrlList = new HashSet<>();
        Set<String> successUrlList = new HashSet<>();
        Set<String> exceptionUrlList = new HashSet<>();

        for (String gotoUrl : urlList) {
            try {
                Document document = getDocumentByFirefoxSelenium(gotoUrl);
                if (document == null) {
                    documentIsNullUrlList.add(gotoUrl);
                    continue;
                }
                Element elementById = document.getElementById("app-main");
                if (elementById == null) {
                    appMainIsNullUrlList.add(gotoUrl);
                    continue;
                }

                String text = elementById.text();

                // 写入文件
                String audioFilePath = "/Users/meek/Documents/已经生成的计算机单词/swfit-swfitui-已完成/" + DatetimeUtil.currentEpochMilli() + ".txt";
                FileUtil.writeStringToFile(audioFilePath, text);
                successUrlList.add(gotoUrl);

                // 睡眠2秒
                ThreadUtil.sleepBySeconds(2);
            } catch (Exception e) {
                ExceptionUtil.printStackTraceAsString(e);
                exceptionUrlList.add(gotoUrl);
            }
        }

        String documentIsNullUrlList_path = "/Users/meek/Documents/已经生成的计算机单词/swfit-swfitui-已完成/documentIsNullUrlList.json";
        FileUtil.writeStringToFile(documentIsNullUrlList_path, JsonUtil.toJson(documentIsNullUrlList));

        String appMainIsNullUrlList_path = "/Users/meek/Documents/已经生成的计算机单词/swfit-swfitui-已完成/appMainIsNullUrlList.json";
        FileUtil.writeStringToFile(appMainIsNullUrlList_path, JsonUtil.toJson(appMainIsNullUrlList));

        String successUrlList_path = "/Users/meek/Documents/已经生成的计算机单词/swfit-swfitui-已完成/successUrlList.json";
        FileUtil.writeStringToFile(successUrlList_path, JsonUtil.toJson(successUrlList));

        String exceptionUrlList_path = "/Users/meek/Documents/已经生成的计算机单词/swfit-swfitui-已完成/exceptionUrlList.json";
        FileUtil.writeStringToFile(exceptionUrlList_path, JsonUtil.toJson(exceptionUrlList));


        // 发送通知
        SendWorkWeixinMessageUtil.sendApplicationMessage("Swift 已经抓取完成");
    }


    public List<String> readCsvData(String csvFile) {
        String line;
        String csvSplitBy = ","; // CSV 分隔符

        List<String> urlList = new ArrayList<>(); // 用于存储第一列 URL 的数组
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            // 读取表头行
            br.readLine(); // 如果不需要标题行，读取并跳过它

            while ((line = br.readLine()) != null) {
                // 使用逗号作为分隔符
                String[] columns = line.split(csvSplitBy);

                // 检查数组长度，确保第一列存在数据
                if (columns.length > 0) {
                    // 将第一列的 URL 添加到数组中
                    urlList.add(columns[0].replaceAll("\"", "")); // 去除多余的引号
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        // 输出所有的 URL
        // for (String url : urlList) {
        // System.out.println(url);
        // }
        return urlList;
    }

    public Document getDocumentByFirefoxSelenium(String gotoUrl) {
        System.setProperty("webdriver.firefox.bin", "/Applications/Firefox.app/Contents/MacOS/firefox");
        System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver");
        WebDriver webDriver = null;
        try {
            FirefoxBinary firefoxBinary = new FirefoxBinary();
            // 无浏览器模式
            firefoxBinary.addCommandLineOptions("--headless");
            FirefoxOptions options = new FirefoxOptions();
            options.setBinary(firefoxBinary);
            options.addPreference("general.useragent.override", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:130.0) Gecko/20100101 Firefox/130.0");
            webDriver = new FirefoxDriver(options);

            webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            webDriver.get(gotoUrl);

            // 禁用可能的防爬虫特征（如果抓取不到可以打开这两个注释试试看）
            // options.addPreference("dom.webdriver.enabled", false);
            // options.addPreference("useAutomationExtension", false);

            // 等待页面加载（现在很多网站的抓取都需要采用模拟人的行为）
            Thread.sleep(2000);

            // 模拟用户行为：滚动页面
            ((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0, 1000);");

            String pageHtmlSource = webDriver.getPageSource();// 获取网页全部信息
            return Jsoup.parse(pageHtmlSource);
        } catch (Exception e) {
            log.error("------zch------firefox selenium 抓取报错 {}, <{}>", gotoUrl, e.getMessage());
        } finally {
            // 使用完毕，关闭webDriver
            if (webDriver != null) {
                webDriver.quit();
            }
        }
        return null;
    }

}
